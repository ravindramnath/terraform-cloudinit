## Terraform - Cloud Init Demo

AWS: Provisions an EC2 instance running Ubuntu Server 16.04 LTS - US West (AMI ID based on region)

1. VPC provisioned
2. Subnets provisioned and IP Tables updated accordingly
3. Provisioned an instance with a specific OS version
4. Configured the incoming connection for SSH (port 22) only
5. Password authentication has been disabled
6. As a second layer of security, the server has been configured to only allow  for SSH (port 22) only. All other ports are closed.
8. Firewall enabled
9. Login is certificate based - user public keys need to be signed by CA
10. Root login has been disabled
11. Added custom message to the MOTD
12. Single user created - ubuntu for testing. Keys for this user is devops
13. CA password: ubuntu

## Required

Required: AWS Key+Secret (your own account)

## Commands
terraform init  
terraform plan  
terraform apply  
